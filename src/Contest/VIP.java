package Contest;

class VIP extends TiketKonser {
    // Do your magic here...
     //Construktor untuk tiket jenis CAT,dimana terdapat inheritance dari class TiketKonser 
    //sehigga constructor harus terdapat bagian dari construkctor super class nya
    public VIP(String namaPemesan, String kodePesanan, String tanggalPesanan) {
        super(namaPemesan, kodePesanan, tanggalPesanan, "VIP");
    }
    //Nah untuk yang ini karna dibagian class TIketKonser ada method abstract dan kita mengextends
    //class ini dengan class tersebut 
    //method abstract hitungTotalHarga()
    //harga nya diambil dari method getHargaTiket();
    @Override
    public double hitungTotalHarga() {
        return getHargaTiket();
    }
    //Karna TiketKonser implements harga tiket,maka perlu mengimplementasikan
    //method getHargaTiket()
    //method ini mereturn harga tiket
    @Override
    public double getHargaTiket() {
        return 2000000;
    }
}
package Contest;
//pada bagian pemesanan tiket saya menggunakan exception ini untuk mengatasi kesalah input
//di bagian nomor tiket khususnya
//massage nya langsung dari class parent Exception

class InvalidInputException extends Exception {
    public InvalidInputException(String message) {
        super(message);
    }
}
package Contest;

abstract class TiketKonser implements HargaTiket {
    // Do your magic here...
    //seperti biasa ,ini atribut untuk objek abstrac yang akan diimplementasikan oleh berbagai jenis tiket 
    private String namaPemesan;
    private String kodePesanan;
    private String tanggalPesanan;
    private String namaTiket;

    //merupakan konstruktor yang akan diimplementasikan di class anak ,
    //seperti class CAT8,CAT1 dll
    public TiketKonser(String namaPemesan, String kodePesanan, String tanggalPesanan, String namaTiket) {
        this.namaPemesan = namaPemesan;
        this.kodePesanan = kodePesanan;
        this.tanggalPesanan = tanggalPesanan;
        this.namaTiket = namaTiket;
    }

    //method yang harus diimplementasikan di class anak
    //tujuannya untuk mendapat total harga
    public abstract double hitungTotalHarga();

    //mencetak tiket pesanan para pembeli war tiket coldplay
    //ini akan dipanggil di pemesanan tiket juga
    public void cetakDetailPesanan() {
        System.out.println("==== Detail Pemesanan ====");
        System.out.println("Nama Pemesan   : " + namaPemesan);
        System.out.println("Kode Pesanan   : " + kodePesanan);
        System.out.println("Tanggal Pesanan: " + tanggalPesanan);
        System.out.println("Nama Tiket     : " + namaTiket);
        System.out.printf("Total Harga    : Rp. %,.2f", hitungTotalHarga());
       // System.out.printf("Total Harga: Rp. %,.f " ,hitungTotalHarga());
    }
}
/*
 * UAP PEMLAN 2023
 * DURASI: 120 MENIT
 * TEMPAT PENGERJAAN: DARING/LURING
 * =================================================================
 * Semangat mengerjakan UAP teman-teman
 * Jangan lupa berdoa untuk hasil yang terbaik
 */

package Contest;

import java.util.*;
import java.text.SimpleDateFormat;

public class Main {
    public static void main(String[] args) throws InvalidInputException {
        //Do your magic here...
        //Untuk mengimplementasikan pemesanan di class Main
        //kita perlu membaut objek dari class Pemesanan Tiket 
        //agar kita bisa beli tiket hehe
        PemesananTiket pemesananTiket = new PemesananTiket();
        pemesananTiket.pesanTiket();
    }

    /*
     * Jangan ubah isi method dibawah ini, nama method boleh diubah
     * Method ini dipanggil untuk mendapatkan kode pesanan atau kode booking
     * Panggil method ini untuk kelengkapan mencetak output nota pesanan
     */
    //method ini berguna untuk membuat kode yang terdiri dari 8 karakter
    //menggunakan perulangan 
    //Karakter nya yang akan diacak berasal dari huruf A-7 dan angka 0-9
    //Karakter nantinya di simpan di StringBuilder
    //Tipe data StringBuilder ini unik
    //nilainya dapat diubah tanpa membuat objek baru setiap kali dilakukan operasi penggabungan atau manipulasi
    public static String generateKodeBooking() {
        StringBuilder sb = new StringBuilder();
        String characters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
        int length = 8;

        for (int i = 0; i < length; i++) {
            int index = (int) (Math.random() * characters.length());
            sb.append(characters.charAt(index));
        }

        return sb.toString();
    }

    /*
     * Jangan ubah isi method dibawah ini, nama method boleh diubah
     * Method ini dipanggil untuk mendapatkan waktu terkini
     * Panggil method ini untuk kelengkapan mencetak output nota pesanan
     */

    //method ini berguna untuk mengembalikan waktu berupa tanggal saat ini
    //format nya dd-MM--yyyy untuk mengembalikan waktu berupa angka saja ,bukan seperti nama bulannya langsung
    public static String getCurrentDate() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
        Date currentDate = new Date();
        return dateFormat.format(currentDate);
    }
}
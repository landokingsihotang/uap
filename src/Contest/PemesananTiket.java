package Contest;

import java.util.InputMismatchException;
import java.util.Scanner;

class PemesananTiket {
    // Do your magic here...
    public void pesanTiket() throws InvalidInputException {
        //Scanner untuk bagian input nama pemesan dan nomor tiket
        Scanner scanner = new Scanner(System.in);

        System.out.println("Selamat Datang Di Pemesanan Tiket COLDPLAY");
        System.out.println("Jenis tiket yang tersedia:");
        System.out.println("1. CAT8 (Rp. 500,000)");
        System.out.println("2. CAT1 (Rp. 1,000,000)");
        System.out.println("3. FESTIVAL (Rp. 1,500,000)");
        System.out.println("4. VIP (Rp. 2,000,000)");
        System.out.println("5. VVIP (UNLIMITED EXPERIENCE) (Rp. 3,000,000)");

        //boolean untuk perulangan 
        boolean inputValid = false;
        do {
        try {
            System.out.print("Masukkan nama pemesan: ");
            String namaPemesan = scanner.nextLine();
            System.out.print("Masukkan pilihan tiket (1-5): ");
            int pilihanTiket = scanner.nextInt();
        
            TiketKonser tiket;
            //membuat pengkondisian terkait tiket yang akan dipilih oleh user
            //case akan sesuai dengan nomor tiket di atas
            //setiap nomor yang dipilih akan membuat objek tiket baru 
            //dan pastinya harga tiket beda ya :(
            switch (pilihanTiket) {
                case 1:
                    tiket = new CAT8(namaPemesan, Main.generateKodeBooking(), Main.getCurrentDate());
                    break;
                case 2:
                    tiket = new CAT1(namaPemesan, Main.generateKodeBooking(), Main.getCurrentDate());
                    break;
                case 3:
                    tiket = new FESTIVAL(namaPemesan, Main.generateKodeBooking(), Main.getCurrentDate());
                    break;
                case 4:
                    tiket = new VIP(namaPemesan, Main.generateKodeBooking(), Main.getCurrentDate());
                    break;
                case 5:
                    tiket = new VVIP(namaPemesan, Main.generateKodeBooking(), Main.getCurrentDate());
                    break;
                default:
                //ini digunakan apabila user menginput angka lebih kecil 1 dan lebih dari 5
                    throw new InvalidInputException("Pilihan tiket tidak valid!");
            }
        
            tiket.cetakDetailPesanan();
        //apabila tidak ada exception,maka inputValid akan true ,dan perulangan berhenti
        //apabila masih ada exception yang kena
        //maka akan melakukan perulangan untuk memasukkan nama pemesan
        //dan nomor tiket ,sampai nama dan nomor tiket nya benar
            inputValid = true;  
        
        } catch (InvalidInputException e) {
            System.out.println("Terjadi kesalahan dalam input: " + e.getMessage());
        } catch (InputMismatchException e) {
            System.out.println("Input tidak valid! Pastikan memasukkan angka untuk pilihan tiket.");}
            scanner.nextLine();
        } while (!inputValid);
        }}
        